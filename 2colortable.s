
.include "globals.inc"

.segment "RODATA"

.macro shades base
   .byte (base+$0),(base+$2),(base+$4),(base+$6)
   .byte (base+$8),(base+$a),(base+$c),(base+$e)
   .byte (base+$e),(base+$c),(base+$a),(base+$8)
   .byte (base+$6),(base+$4),(base+$2),(base+$0)
.endmacro

   .align 256

paltab:
   shades $00
   shades $20
   shades $20
   shades $40
   shades $60
   shades $80
   shades $a0
   shades $c0
   shades $d0
   shades $b0
   shades $90
   shades $70
   shades $50
   shades $30
   shades $30
   shades $20

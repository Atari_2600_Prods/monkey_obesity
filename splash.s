
.include "vcs.inc"
.include "globals.inc"

.segment "RODATA"

.if splashtype = 3

ntscpaltable:
; eor table
  .byte $00 ; $0x
  .byte $30 ; $2x
  .byte $00 ; $2x
  .byte $70 ; $4x
  .byte $20 ; $6x
  .byte $D0 ; $8x
  .byte $C0 ; $Ax
  .byte $B0 ; $Cx
  .byte $50 ; $Dx
  .byte $20 ; $Bx
  .byte $30 ; $9x
  .byte $C0 ; $7x
  .byte $90 ; $5x
  .byte $E0 ; $3x
  .byte $D0 ; $3x
  .byte $D0 ; $2x

.endif

.if splashtype = 4

ntscpaltable:
; ora table
  .byte $00
  .byte $20
  .byte $20
  .byte $40
  .byte $60
  .byte $80
  .byte $A0
  .byte $C0
  .byte $D0
  .byte $B0
  .byte $90
  .byte $70
  .byte $50
  .byte $30
  .byte $30
  .byte $20

.endif

.segment "ZEROPAGE"

frmcnt = localramstart+0

; gfx are written to $90-$ec

.segment "CODE"

.if splashtype > 0

splash:
   ldy #$00
   lda frmcnt
   bne @noinit
   iny            ; initialize
   sty CTRLPF
   iny
   sty AUDC0
   ldx #$1f       ; X on purpose, will be used in copy below
   stx AUDF0      ; setup initial (motor) sound effect
   lda #$fc       ; number of remaining frames for intro and volume
   sta AUDV0
   sta frmcnt

   lda #%00110000 ; initial bit pattern for atari logo
@gfxloop:
   tay            ; also number of lines generated
   ora #$80       ; add center bar
@loop1:
   sta $71,x      ; copy to $90-$ec, X starts with 1, see above
   inx
   dey
   bne @loop1     ; from here on Y=$00, assumed below
   and #$7f       ; remove center bar
   lsr            ; shift playfield data (and height)
   cmp #%00000001 ; as long as there's space left for gfx
   bne @gfxloop

@noinit:
   cmp #$fb             ; on first frame
   bne @notmagicframe   ; check for fire button on left joystick
   bit INPT4            ; do nothing until released
   bpl @waitbuttonup    ; (used for getting sync on recording)
@notmagicframe:
   dec frmcnt
@waitbuttonup:
   jsr waitvblank
   
   ldx frmcnt           ; any frames left
   cli                  ; no: skip to next part
   beq @exit            ; and don't draw anything
   sei
@notdone:
   cpx #$10
   bcs @nomute          ; on the last frames
   sty AUDV0            ; turn off sound
@nomute:
   cpx #$50             ; see if we want "jumping" sound effect
   bcs @disploop
   lda #$06 
   sta AUDC0
   txa
   ldx #$50             ; for drawing, set it back
   sbc #$2f
   bpl @norev
   eor #$1f             ; second half, tone goes down again
@norev:
   sta AUDF0

@disploop:
   cpx #$90
   tya
   bcc @cleargfx
   lda $00,x
@cleargfx:
   sta WSYNC
   sta PF2              ; set precalc'ed gfx data
   txa                  ; generate "rainbow" colors data

.if splashtype = 1
   ; just use PAL rainbow (looks bad)
   asl

.elseif splashtype = 2
   ; SvOlli's idea: calculate ntsc->pal color conversion on-the-fly
   ; short, but can't be reused

; 2 %0010 <- 2 %0010
; 4 %0100 <- 3 %0011
; 6 %0110 <- 4 %0100
; 8 %1000 <- 5 %0101
; A %1010 <- 6 %0110
; C %1100 <- 7 %0111
   and #$07
   sta $82
   txa
   asl ; a > $80 => sec
   bmi @upper
   and #$70
   sbc #$10
   bpl @skip
@upper:
; D %1101 <- 8 %1000
; B %1011 <- 9 %1001
; 9 %1001 <- A %1010
; 7 %0111 <- B %1011
; 5 %0101 <- C %1100
; 3 %0011 <- D %1101
   and #$70
   eor #$ff
   adc #$68
@skip:   
   ora $82
   asl

.elseif splashtype = 3
   ; Omegamatrix' idea: use an eor color conversion table
   ; can be reused in certain situations

   lsr
   lsr
   lsr
   and #$0f
   tay
   txa
   asl
   eor ntscpaltable,y
   ldy #$00

.elseif splashtype = 4
   ; Omegamatrix' idea: use an ora color conversion table
   ; 2 bytes longer than #3, but can be easier reused

   lsr
   lsr
   lsr
   and #$0f
   tay
   txa
   asl
   and #$0f
   ora ntscpaltable,y
   ldy #$00

.endif

   sta COLUPF
   inx
   cpx #$ee ; last byte = $00 to clear PF2
   bcc @disploop

@exit:
   
   jsr waitscreen
   jmp waitoverscan

.endif

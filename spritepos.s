
.include "globals.inc"
.include "vcs.inc"

.segment "CODE"


spritepos:
; x = sprite to set
; a = position
   sta WSYNC   ;        0
   sta HMCLR   ;   3 =  3
   sec         ;   2 =  5
@loop:
   sbc #$0f    ; x*2 =  7
   bcs @loop   ; x*3 =  5 (-1 = 9)
   eor #$07    ;   2 = 11
   asl         ;   2 = 13
   asl         ;   2 = 15
   asl         ;   2 = 17
   asl         ;   2 = 19
   sta RESP0,x ;   4 = 23 + 5*x
   sta HMP0,x  ;   4 = 27 + 5*x
   sta WSYNC   ;
   sta HMOVE
   rti

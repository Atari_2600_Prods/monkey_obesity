
; configuration parameters

; 0=off,palette correction:1=none,2=svolli,3=omegamatrix eor,4=omegamatrix ora
.define splashtype 2

; timer values for vblank and overscan are for TIM64TI, screen is for TIM1KTI
; PAL values, determined by trial an error
; values are calculated by 2*timer value | timer index (0: TIM1KTI, 1: TIM64TI)
.define TIMER_VBLANK   1+2*$2a ; ~2688 cycles
.define TIMER_SCREEN   0+2*$13
.define TIMER_OVERSCAN 1+2*$16 ; ~1280 cycles

; all symbols visible to other source files

; 0pglobal.s
.globalzp schedule
.globalzp temp8
.globalzp temp16 ; 2 bytes
.globalzp psmkAttenuation
.globalzp psmkBeatIdx
.globalzp psmkPatternIdx
.globalzp psmkTempoCount
; RAM used for each part separately starts at $88
.globalzp localramstart

; 1sinustable127.s
.global   sinustab

; 2colortable.s
.global   paltab

; charset.s
.global   charset

; plotter.s
.global   plotter

; spritepos.s
.global   spritepos

; slocumplayer21.s
.global   psmkPlayer

; textscroll.s
.global   calcscroll
.globalzp scroll

; main.s
.global   reset
.global   waitvblank   ; called with jsr; x,y untouched
.global   waitscreen   ; called with jsr; x,y untouched
.global   waitoverscan ; called with jmp
; alternative versions, that take use A instead of default for delay
.global   waitvblanka
.global   waitscreena
.global   waitoverscana

; splash.s
.if splashtype > 0
.global   splash
.if (splashtype = 3) || (splashtype = 4)
.global   ntscpaltable
.endif
.endif

.linecont +
   .define partsaddrlist \
   plotter-1, \
   reset-1
.linecont -



.include "vcs.inc"
.include "globals.inc"

.segment "ZEROPAGE"

frmcnt = localramstart+0
table  = sinustab
offset = localramstart+1
patidx = localramstart+2

.segment "RODATA"
goodpatterns:
   .byte $17,$3F,$1D,$53,$1F,$57,$25,$6D
   .byte $29,$7B,$2B,$7D,$31,$8F,$33,$93
   .byte $99,$35,$9B,$00

.segment "CODE"

plotter:
   lda frmcnt
   bne @noinc
   ldx patidx
   inx
@reset:
   stx patidx
   lda goodpatterns,x
   bne @patok
   ldx #$00
   beq @reset
@patok:
   sta offset
@noinc:
   inc frmcnt
   lda #$02
   sta COLUPF

   lda #$07
   sta NUSIZ0
   sta NUSIZ1
 
   lda #$04
   sta COLUP0
   lda #$02
   sta COLUP1

   lda frmcnt
   asl
   tay
   lda sinustab,y
   lsr
   clc
   adc #$1c
   pha
   ldx #$00
   ; jsr spritepos
   brk
   nop
   pla
   ;clc
   adc #$04
   inx
   ; jsr spritepos
   brk
   nop

   lda #$00
   sta GRP0
   sta GRP1

   jsr calcscroll
   jsr waitvblank

   ldy frmcnt
   lda table,y
   ldx #$c2
@dotloop:
   pha
   sec
   lda #$00         ; 2
   sta WSYNC        ; 3
   sta HMCLR        ; 3= 3
   sta ENABL        ; 3= 6
   pla;table,y      ; 4=10
@posloop1: ; pos <= $7f -> 8+1 loops
   sbc #$0f         ; 2*9=28
   bcs @posloop1    ; 3*8+2=54
   eor #$07         ; 2=56
   asl              ; 2=58
   asl              ; 2=60
   asl              ; 2=62
   asl              ; 2=64
   sta RESBL+$100   ; 2=66
   sta HMBL         ; 3=69

   sta WSYNC        ; 3=72/0
   sta HMOVE        ; 3= 3
   lda #$02         ; 2= 5
   sta ENABL        ; 3= 8
   ; optional stuff can happen here...
   lda scroll+$3e,x
   sta GRP0
   lda paltab,y
   sta COLUPF
   tya
   adc #$40
.if 1
   and #$80
   rol
   rol
   rol
   rol
.else
   bmi @back1
   lda #$01
   .byte $2c
@back1:
   lda #$05
.endif
   sta CTRLPF
   tya              ; 2=10
   clc
   adc offset       ; 2=10
   tay              ; 2=10
   lda table,y
   
   pha
   sec
   lda #$00         ; 2
   sta WSYNC        ; 3
   sta HMCLR        ; 3= 3
   sta ENABL        ; 3= 6
   pla;table,y      ; 4=10
@posloop2: ; pos <= $7f -> 8+1 loops
   sbc #$0f         ; 2*9=28
   bcs @posloop2    ; 3*8+2=54
   eor #$07         ; 2=56
   asl              ; 2=58
   asl              ; 2=60
   asl              ; 2=62
   asl              ; 2=64
   sta RESBL+$100   ; 2=66
   sta HMBL         ; 3=69

   sta WSYNC        ; 3=72/0
   sta HMOVE        ; 3= 3
   lda #$02         ; 2= 5
   sta ENABL        ; 3= 8
   ; optional stuff can happen here...
   lda scroll+$3e,x
   sta GRP1
   lda paltab,y
   sta COLUPF
   tya
   adc #$40
.if 1
   and #$80
   rol
   rol
   rol
   rol
.else
   bmi @back2
   lda #$01
   .byte $2c
@back2:
   lda #$05
.endif
   sta CTRLPF
   tya              ; 2=10
   clc
   adc offset       ; 2=10
   tay              ; 2=10
   lda table,y
   ;and #$7f
   inx              ; 2=12
   bne @dotloop     ; 3=15
   sta WSYNC
   stx ENABL
   stx GRP0
   stx GRP1

   jsr waitscreen
   jsr psmkPlayer
   jmp waitoverscan

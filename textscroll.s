
.include "vcs.inc"
.include "globals.inc"

.segment "ZEROPAGE"

temp    = localramstart + 3
textvec = localramstart + 5
charpos = localramstart + 7
scroll  = localramstart + 8 ; size=66

.segment "RODATA"
text:
   .byte "       "
   .byte "O HI THERE BOTBRRRS * THANKS FOR CHECKING OUT MY WINTER CHIP X TIA ENTRY ***"
   .byte " GREETZ AND RESPECT TO AKAREYON * AJI * B00DAW * B-KNOX * CCE * CHIPCHAMP * GARVALF * GOTO&PLAY * FLAMINGLOG * HERTZDEVIL * INTROSPEC * JAC! * KEFFIE * KEONIE * KFF * LINDE * MEGA9MAN * MR BEEP"
   .byte " * MK7 * MKSTAR26 * NOOB * PUKE7 * RAPH * SLIMBOL * TAYLE * TILDE * TRONIMAL * VINCMG * WARLORD * YERZ * ZILLAH * AND EVERYBODY ELSE THAT I'M FORGOTTING ATM. ALSO: MAD RESPECT TO ALL VCS CODERS! *** "
   .byte "MUSIC: IRRLICHT PROJECT *** CODE: SVOLLI *** MUSIC PLAYER: P. SLOCUM                                        ALL NOOBZ!"
   .byte "              ",0

.segment "CODE"

textreset:
   lda #<(text-1)
   sta textvec
   lda #>(text-1)
   sta textvec+1
   rts

calcscroll:
   lda textvec+1
   bne @txtok
   jsr textreset
@txtok:
   ldx #$00
@moveloop:
   lda scroll+1,x
   sta scroll,x
   inx
   cpx #65
   bne @moveloop

   ldy charpos
   dey
   bpl @cpok
@reload:
   ldy #$07
   inc textvec
   bne @cpok
   inc textvec+1
@cpok:
   sty charpos
   ldy #$00
   lda (textvec),y
   bne @notdone
   jsr textreset
   bne @reload
@notdone:
   ldy charpos
   sec
   sbc #$20
   sta temp
   lda #$00
   sta temp+1
   asl temp
   rol temp+1
   asl temp
   rol temp+1
   asl temp
   rol temp+1
   lda temp
   adc #<(charset)
   sta temp
   lda temp+1
   adc #>(charset)
   sta temp+1
   lda (temp),y
   sta scroll+65
   rts
